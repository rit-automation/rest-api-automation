package bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Employee {
private String name;
private String job;
private int id;
private String createdAt;
}
