public interface TestData {
    String url = "https://reqres.in/api";
    String urlInternalServerError = "http://httpstat.us/500";
}
