package restassured;

import bean.Employee;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.json.simple.JSONObject;

import static io.restassured.RestAssured.given;

public class InternalServerErrorResponse {

    public static Response postResponse(String url, Employee employee) {

        JSONObject request = new JSONObject();

        request.put("name", employee.getName());
        request.put("job", employee.getJob());

        return given()
                .header("Content-Type", "application/json")
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request.toJSONString())
                .when().post(url)
                .then().extract().response();
    }
}
