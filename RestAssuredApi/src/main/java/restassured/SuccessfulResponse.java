package restassured;

import bean.Employee;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.json.simple.JSONObject;

public class SuccessfulResponse extends RestAssured {

    public static Response getResponse(String url) {
     return given()
              .when().get(url + "?page=2")
              .then().extract().response();
    }

    public static Response postResponse( String url, Employee employee) {

        JSONObject request = new JSONObject();

        request.put("name", employee.getName());
        request.put("job", employee.getJob());

        return  given()
                .header("Content-Type", "application/json")
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request.toJSONString())
                .when().post(url)
                .then().extract().response();
    }

    public static Response putResponse(String url, Employee employee) {
        JSONObject request = new JSONObject();
        request.put("name", employee.getName());
        request.put("job", employee.getJob());

        return given()
                .header("Content-Type", "application/json")
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request.toJSONString())
                .when().put(url + "/" + employee.getId())
                .then().extract().response();
    }

    public static Response patchResponse(String url, Employee employee) {
        JSONObject request = new JSONObject();
        request.put("name", employee.getName());
        request.put("job", employee.getJob());

        return given()
                .header("Content-Type", "application/json")
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request.toJSONString())
                .when().patch(url + "/"  + employee.getId())
                .then().extract().response();
    }

    public static Response deleteResponse(String url, Employee employee) {

        return given()
                .when().delete(url + "/"  + employee.getId())
                .then().extract().response();
    }
}
