package restassured;

import bean.Employee;
import bean.Login;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.json.simple.JSONObject;

public class ClientErrorResponse extends RestAssured {

    public static Response getResponse(String url, int employeeId) {
        return given()
                .when().get(url + "/" + employeeId)
                .then().extract().response();
    }

    public static Response postResponse(String url, Login login) {

        JSONObject request = new JSONObject();

        request.put("email", login.getEmail());

        return given()
                .header("Content-Type", "application/json")
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request.toJSONString())
                .when().post(url)
                .then().extract().response();
    }
}
