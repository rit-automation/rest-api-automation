import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import org.junit.After;
import org.junit.Before;

public class BaseTest {
    protected static ExtentHtmlReporter htmlReporter;
    protected static ExtentReports extentReports;
    protected static ExtentTest test;

    @Before
    public void initialSetup() {
        if (extentReports==null){
            htmlReporter = new ExtentHtmlReporter("report.html");
            extentReports = new ExtentReports();
            extentReports.attachReporter(htmlReporter);
        }
    }

    @After
    public void quitBrowser() {
        extentReports.flush();
    }
}
