import bean.Employee;
import bean.Login;
import org.junit.Assert;
import org.junit.Test;
import restassured.ClientErrorResponse;

public class RestAssuredClientErrorTest extends BaseTest implements TestData {

    @Test
    public void testGetResponse() {
        try {
            test = extentReports.createTest("Retrieve user details",
                    "Unsuccessful user details retrieve");
            Employee employee = new Employee();
            employee.setId(23);

            int statusCode = ClientErrorResponse.getResponse(url + "/unknown",
                    employee.getId()).getStatusCode();
            Assert.assertEquals(statusCode, 404);
            test.pass(String.valueOf(statusCode));
        } catch (AssertionError ex) {
            test.fail(ex.getMessage());
            ex.printStackTrace();
        } catch (Exception ex) {
            test.fail(ex.getMessage());
            ex.printStackTrace();
        }
    }

    @Test
    public void testPostResponse() {
        try {
            test = extentReports.createTest("User Login",
                    "Unsuccessful user login");

            Login login = new Login();
            login.setEmail("peter@klaven");

            int statusCode = ClientErrorResponse.postResponse(url
                    + "/login", login).getStatusCode();
            Assert.assertEquals(statusCode, 400);
            test.pass(String.valueOf(statusCode));
        } catch (AssertionError ex) {
            test.fail(ex.getMessage());
            ex.printStackTrace();
        } catch (Exception ex) {
            test.fail(ex.getMessage());
            ex.printStackTrace();
        }
    }
}
