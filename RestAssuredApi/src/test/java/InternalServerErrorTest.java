import bean.Employee;
import org.junit.Assert;
import org.junit.Test;
import restassured.InternalServerErrorResponse;

public class InternalServerErrorTest extends BaseTest implements TestData {

    @Test
    public void testPostResponse() {
        try {
            test = extentReports.createTest("Enter employee details",
                    "Unsuccessful enter employee details due to server error");

            Employee employee = new Employee();
            employee.setName("Test Employee" + System.currentTimeMillis());
            employee.setJob("leader");

            int statusCode = InternalServerErrorResponse.postResponse(TestData
                    .urlInternalServerError, employee).getStatusCode();
            Assert.assertEquals(statusCode, 500);
        } catch (AssertionError ex) {
            test.fail(ex.getMessage());
            ex.printStackTrace();
        } catch (Exception ex) {
            test.fail(ex.getMessage());
            ex.printStackTrace();
        }
    }
}