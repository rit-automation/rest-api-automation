import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

import java.util.logging.Logger;

/**
 * This Test Runner Class is use integrate all api test and runs at once.
 */
public class TestRunner {

    public static void main(String args[]) {
        Result result = JUnitCore.runClasses(RestAssuredSucceedTest.class,
                RestAssuredClientErrorTest.class, InternalServerErrorTest.class);

        Logger.getLogger("Total number of tests " + result.getRunCount());
    }
}