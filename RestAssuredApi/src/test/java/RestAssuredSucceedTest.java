import bean.Employee;
import org.junit.Assert;
import org.junit.Test;
import restassured.SuccessfulResponse;

public class RestAssuredSucceedTest extends BaseTest implements TestData {

    @Test
    public void testGetResponse() {
        try {
            test = extentReports.createTest("Retrieve users details",
                    "Successful user details retrieve");
            int statusCode = SuccessfulResponse.getResponse(url + "/users").getStatusCode();
            Assert.assertEquals(statusCode, 200);
        } catch (AssertionError ex) {
            test.fail(ex.getMessage());
            ex.printStackTrace();
        } catch (Exception ex) {
            test.fail(ex.getMessage());
            ex.printStackTrace();
        }
    }

    @Test
    public void testPostResponse() {
        try {
            test = extentReports.createTest("Adding a new user",
                    "Successful adding a new user");

            Employee employee = new Employee();
            employee.setName("Test Employee" + System.currentTimeMillis());
            employee.setJob("leader");

            int statusCode = SuccessfulResponse.postResponse(url
                    + "/users", employee).getStatusCode();
            Assert.assertEquals(statusCode, 201);
        } catch (AssertionError ex) {
            test.fail(ex.getMessage());
            ex.printStackTrace();
        } catch (Exception ex) {
            test.fail(ex.getMessage());
            ex.printStackTrace();
        }
    }

    @Test
    public void testPutResponse() {
        try {
            test = extentReports.createTest("Update existing user detail data set",
                    "Successful update existing user detail data set");
            Employee employee = new Employee();
            employee.setName("Test Employee" + System.currentTimeMillis());
            employee.setJob("Lawyer");
            employee.setId(2);

            int statusCode = SuccessfulResponse.putResponse(url + "/users", employee)
                    .getStatusCode();
            Assert.assertEquals(statusCode, 200);
        } catch (AssertionError ex) {
            test.fail(ex.getMessage());
            ex.printStackTrace();
        } catch (Exception ex) {
            test.fail(ex.getMessage());
            ex.printStackTrace();
        }
    }

    @Test
    public void testPatchResponse() {
        try {
            test = extentReports.createTest("Update existing user detail data field",
                    "Successful update existing user detail data field");

            Employee employee = new Employee();
            employee.setName("Test Employee" + System.currentTimeMillis());
            employee.setJob("Engineer");
            employee.setId(3);

            int statusCode = SuccessfulResponse.patchResponse(url + "/users", employee)
                    .getStatusCode();
            Assert.assertEquals(statusCode, 200);
        } catch (AssertionError ex) {
            test.fail(ex.getMessage());
            ex.printStackTrace();
        } catch (Exception ex) {
            test.fail(ex.getMessage());
            ex.printStackTrace();
        }
    }

    @Test
    public void testDeleteResponse() {
        try {
            test = extentReports.createTest("Delete existing user details set",
                    "Successful delete existing user details set");

            Employee employee = new Employee();
            employee.setName("Test Employee" + System.currentTimeMillis());
            employee.setJob("Engineer");
            employee.setId(3);

            int statusCode = SuccessfulResponse.deleteResponse(url + "/users", employee)
                    .getStatusCode();
            Assert.assertEquals(statusCode, 204);
        } catch (AssertionError ex) {
            test.fail(ex.getMessage());
            ex.printStackTrace();
        } catch (Exception ex) {
            test.fail(ex.getMessage());
            ex.printStackTrace();
        }
    }
}
