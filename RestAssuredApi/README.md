# Rest assured api test

### Table of Content
- [Introduction](#Introduction)
- [Prerequisites](#Prerequisites)
- [Running tests locally](#Running-tests-locally)
- [Technology stack](#Technology-stack)

## Introduction

This project contains UI functional and visual automated tests for Shop demo qa

### Prerequisites
- [Java 19](https://download.java.net/java/early_access/loom/docs/api/)
- [Maven](https://maven.apache.org/)
- [RestAssured](https://www.javadoc.io/doc/io.rest-assured/rest-assured/4.3.1/io/restassured/RestAssured.html)
- [Chrome browser has to be installed](https://www.google.com/chrome/?brand=CHBD&gclid=Cj0KCQjwgJv4BRCrARIsAB17JI7-EIZCQfo4Je5O0LNbMME2sYLjDzlcpM6zeRW81u3ehLyeK8Ebz7MaAnnIEALw_wcB&gclsrc=aw.ds)
- [Setup default IDE formatting tool](https://gitlab.com/rit-automation/web-functional-automation/wiki/Setup-default-IDE-formatting-tool)
- [Extent report](https://www.extentreports.com/docs/versions/5/net/index.html)


# Run all tests
Right click on TestRunner class and run all Test cases

# Run single test
Right click on Relevant Api Test class and run the Test which you expect to run
Test file location:- "RestAssuredApi/src/test/java"


### Technology stack
- [Java 19](https://download.java.net/java/early_access/loom/docs/api/)
- [Maven](https://maven.apache.org/)
- [RestAssured](https://www.javadoc.io/doc/io.rest-assured/rest-assured/4.3.1/io/restassured/RestAssured.html)
- [JUnit](https://junit.org/junit4/javadoc/latest/)
- [Extent report](https://www.extentreports.com/docs/versions/5/net/index.html)

### Report generation
# Right click on "report.html" file and "Open IN -> Browser -> Chrome"
